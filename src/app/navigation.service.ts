import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class NavigationService {
  
  // navigation service properties	
  initialPage :boolean;	
  constructor() { }

  getInitialPage(){
  	return this.initialPage;
  }

  setInitialPage(init : boolean){
  	this.initialPage = init;
  }
}
