import { Component, OnInit } from '@angular/core';
import {Hostname} from '../hostname';

@Component({
  selector: 'app-hostname',
  templateUrl: './hostname.component.html',
  styleUrls: ['./hostname.component.css']
})
export class HostnameComponent implements OnInit {
  hostname : Hostname ={
  	clientTextID : 'YYC-Lab'
  } 	

  constructor() { }

  ngOnInit() {
  }

}
