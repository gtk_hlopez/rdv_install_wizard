import { Component } from '@angular/core';
// Title service imported 
import { Title } from '@angular/platform-browser';
import {NavigationService} from './navigation.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
	
	public constructor(private titleService: Title, private navigationService:NavigationService){}

	public setTitle (newTitle: string){
		this.titleService.setTitle(newTitle);
	}

  title = 'Install Wizard';

  ngOnInit() { 
  	// needed to render the title in the title tab
  	this.titleService.setTitle(this.title);
  	this.navigationService.setInitialPage(true);

  	 }

  getInitialPage(){
  	return this.navigationService.getInitialPage();
  }	 

}
