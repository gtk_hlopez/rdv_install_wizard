import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms'; // NgModel is in FormsModule


import { AppComponent } from './app.component';
import { WanComponent } from './wan/wan.component';
import { UrlComponent } from './url/url.component';
import { AppRoutingModule } from './app-routing.module';
import { WanMtkComponent } from './wan-mtk/wan-mtk.component';
import { HostnameComponent } from './hostname/hostname.component';
import { ManagementvlansComponent } from './managementvlans/managementvlans.component';

@NgModule({
  declarations: [
    AppComponent,
    WanComponent,
    UrlComponent,
    WanMtkComponent,
    HostnameComponent,
    ManagementvlansComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
