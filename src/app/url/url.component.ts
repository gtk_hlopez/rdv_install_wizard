import { Component, OnInit } from '@angular/core';
import {Url} from '../url';
import {NavigationService} from '../navigation.service';

@Component({
  selector: 'app-url',
  templateUrl: './url.component.html',
  styleUrls: ['./url.component.css']
})
export class UrlComponent implements OnInit {

  url: Url ={
  	clientID :'100',
  	port : '5000'
  }	
  constructor(private navigationService : NavigationService) { }

  ngOnInit() {
    this.navigationService.setInitialPage(false);
  }

}
