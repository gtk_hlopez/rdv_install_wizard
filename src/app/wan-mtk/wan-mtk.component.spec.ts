import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WanMtkComponent } from './wan-mtk.component';

describe('WanMtkComponent', () => {
  let component: WanMtkComponent;
  let fixture: ComponentFixture<WanMtkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WanMtkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WanMtkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
