import { Component, OnInit } from '@angular/core';
import {WanMtk} from '../wan-mtk';

@Component({
  selector: 'app-wan-mtk',
  templateUrl: './wan-mtk.component.html',
  styleUrls: ['./wan-mtk.component.css']
})
export class WanMtkComponent implements OnInit {

  wanMtk :WanMtk= {
  	ipAddress : '216.123.84.36',
  	gateway: '216.123.84.33',
  	netmask: '255.255.255.224',
  	priDNS : '8.8.8.8',
  	secDNS: '8.8.4.4'  	
  }	

  constructor() { }

  ngOnInit() {
  }

}
