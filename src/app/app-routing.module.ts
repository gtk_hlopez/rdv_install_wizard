import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule,Routes} from '@angular/router' ;
import {WanComponent} from './wan/wan.component';
import {UrlComponent} from './url/url.component';
import {WanMtkComponent} from './wan-mtk/wan-mtk.component';
import {HostnameComponent} from './hostname/hostname.component';
import {ManagementvlansComponent} from './managementvlans/managementvlans.component';

const routes : Routes=[
	{path: 'wan', component: WanComponent},
	{path: 'url', component: UrlComponent},
	{path: 'wan-mtk', component: WanMtkComponent},
	{path: 'hostname', component: HostnameComponent},
	{path: 'managementvlans', component:ManagementvlansComponent}
];

@NgModule({
  exports:[RouterModule],

  declarations: [],
  imports: [
   RouterModule.forRoot(routes)
  ]
})
export class AppRoutingModule { }
