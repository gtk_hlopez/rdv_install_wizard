export class WanMtk{
	ipAddress : string;
	gateway: string;
	netmask: string;
	priDNS: string;
	secDNS: string;
}