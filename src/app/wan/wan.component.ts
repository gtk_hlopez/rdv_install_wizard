import { Component, OnInit } from '@angular/core';
import {Wan} from '../wan';
import {NavigationService} from '../navigation.service';

@Component({
  selector: 'app-wan',
  templateUrl: './wan.component.html',
  styleUrls: ['./wan.component.css']
})
export class WanComponent implements OnInit {
  
  wan  : Wan = {
  	ipAddress : '216.123.84.37',
  	gateway: '216.123.84.33',
  	netmask: '255.255.255.224'
  }	;
  constructor(private navigationService : NavigationService) { }

  ngOnInit() {
  	this.navigationService.setInitialPage(false);
  }

}
