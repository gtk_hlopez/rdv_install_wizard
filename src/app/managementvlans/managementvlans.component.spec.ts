import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagementvlansComponent } from './managementvlans.component';

describe('ManagementvlansComponent', () => {
  let component: ManagementvlansComponent;
  let fixture: ComponentFixture<ManagementvlansComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManagementvlansComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagementvlansComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
