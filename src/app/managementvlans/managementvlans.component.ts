import { Component, OnInit } from '@angular/core';
import {Managementvlan} from '../managementvlan';

@Component({
  selector: 'app-managementvlans',
  templateUrl: './managementvlans.component.html',
  styleUrls: ['./managementvlans.component.css']
})
export class ManagementvlansComponent implements OnInit {

  managementvlans : Managementvlan[] =[
  {name: 'brLAN100',
  ipaddress:'10.49.99.0',
  netmask:'255.255.255.0'}
  ];

  adding: boolean = false;

  constructor() { }

  ngOnInit() {
  }

  addingVlan(){
  	this.adding = true;
  }

  addVlan(nam : string, ip: string, net: string){
  	 let mv = new Managementvlan();
  	 mv.name = nam;
  	 mv.ipaddress= ip;
  	 mv.netmask = net;
  	 this.managementvlans.push(mv);
  	 this.adding = false ;
  }

}
